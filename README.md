# LÖVE Multiplatform

Skeleton project for multiplatform LÖVE game built by GitLab CI.

Currently supporting:

- Windows 64-bit and 32-bit
- Linux x86_64 and i686
- Mac OSX 64-bit
- Android

Currently bundled with [levity](https://bitbucket.org/IoriBranford/levity) tile map library and [game-music-emu](https://bitbucket.org/mpyne/game-music-emu).

## Project setup

1. Fork and clone this project.
2. In `.gitlab-ci.yml`:
   1. Fill in the project info.
   2. If not building for Android, comment out the `android` jobs at the bottom by putting a period before the names: `.full-android` `.demo-android`
   3. If providing a demo, fill in the demo info and uncomment the `demo` jobs by removing periods before the names.
3. Add game assets in `game` directory.
4. If using levity:
   1. In `game/main.lua` set the first map file.
   2. In `game/gamefiles-full.txt` list map files to include in the game.
   3. If providing a demo version, write `game/gamefiles-demo.txt` listing map files to include in the demo.
5. If not using levity, edit `game/make-game.lua` to find and zip all the game assets.
6. Make icons.
   1. Windows: `appicon.ico`
      1. http://icoconvert.com/
   2. Linux: `love-appimage/game.svg`
      1. https://inkscape.org
   3. Mac: to do
   4. Android: `love-android/app/src/main/res/drawable-*dpi/appicon.png`
7. If building for Android, make Android signing key.
   1. Generate keystore file using `keytool` which is part of Java JDK or JRE. https://developer.android.com/studio/publish/app-signing#signing-manually
   2. Overwrite `love-android/app/release.keystore` with your keystore file.
   3. In your project page Settings > CI/CD > Variables, set `KEYSTORE_ALIAS` and `KEYSTORE_PASSWORD` variables to your keystore's alias and password.
8. Commit and push.

## Publishing to itch.io

1. Download all build artifacts you want to publish. Save in project directory.
2. Set up butler. https://itch.io/docs/butler/
3. In `publish-itch.sh` set `ORG` variable to your itch username.
4. Run `publish-itch.sh <version>` where `version` is the version number you are publishing.
   1. In Windows it should be runnable with Git Bash.